#!/bin/bash

SCRIPTFILE=/local/repository/start-epc-in.sh

# See start-epc-impl.sh for how epc.lock file is created.
sudo rm /tmp/epc.lock.*
# Delete any existing tmux session for srsepc started by this user.
tmux kill-session -t epc 2> /dev/null
sudo pkill srsepc
while true; do
  pgrep srsepc &> /dev/null
  if [ $? == 1 ]; then
    break
  fi
  sleep 1
done

tmux new-session -d -s epc "bash $SCRIPTFILE; sleep 300"
