#!/bin/bash

SCRIPTFILE=/local/repository/start-enb-in.sh

# Delete any existing tmux session for srsenb started by this user.
sudo rm /tmp/enb.lock.*
tmux kill-session -t enb 2> /dev/null
sudo pkill srsenb
while true; do
  pgrep srsenb &> /dev/null 
  if [ $? == 1 ]; then
    break
  fi
  sleep 1
done

tmux new-session -d -s enb "bash $SCRIPTFILE; sleep 300"
