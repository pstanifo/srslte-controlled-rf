#!/bin/bash
# Restarts the srsLTE eNodeB and UE instances that were started with start-enb-impl.sh and start-ue-impl.sh
# This is used when srsLTE is used with ZeroMQ. The ZeroMQ implementation is not reliable when either the
# eNodeB or UE is restarted by itself so we should always start both together.

ENB_SCRIPTFILE=/local/repository/start-enb-in.sh
UE_SCRIPTFILE=/local/repository/start-ue-in.sh

sudo pkill srsue

# Delete any existing tmux session for srsenb started by this user.
sudo rm /tmp/enb.lock.*
tmux kill-session -t enb 2> /dev/null
sudo pkill srsenb

while true; do
  pgrep srsue &> /dev/null
  if [ $? == 1 ]; then
    break
  fi
  sleep 1
done

while true; do
  pgrep srsenb &> /dev/null
  if [ $? == 1 ]; then
    break
  fi
  sleep 1
done

tmux new-session -d -s enb "bash $ENB_SCRIPTFILE; sleep 300"
bash $UE_SCRIPTFILE&

