#!/bin/bash
# Keeping pinging the EPC from the UE e.g. 192.168.100.1
# Parameter 1: Where to ping

nohup ping -D -i 5 -W 2 $1 | tee /tmp/ping-test.out&
