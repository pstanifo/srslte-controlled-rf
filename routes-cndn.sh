#!/usr/bin/env bash

# To be run on the `cndn` compute node in the "scenario 2" profile.
# This adds a route back to the RAN subnet (e.g. 172.16.0.0/24) via the EPC

# Parameter 1: Slice Number (1 or 2)
# Parameter 2: EPC node name. If there is no EPC node, use the eNodeB node running EPC software.
# Parameter 3: RAN Subnet Address e.g. 172.16.0.0/24

MYPATH=$(realpath $0 | xargs dirname)
EPC_NODENAME=$2
RAN_SUBNET=$3
EPC_IPADDR=$(getent hosts $EPC_NODENAME | cut -s -d ' ' -f 1)
ERR=0

sudo ip route add $RAN_SUBNET via $EPC_IPADDR || ERR=1

if [[ $ERR == 1 ]]; then
	echo $(basename $0) "Failed to add routes!"
else
	echo $(basename $0) "Finished adding routes."
fi | $MYPATH/log.sh

exit $ERR
