#!/usr/bin/env python
# Share-5G srsLTE environment with one to three "slices" using RF with portable endpoints, controlled RF and simulated RF.
"""Use this profile to instantiate an end-to-end LTE network in an environment containing a combination of OTA RF with portable endpoints, controlled RF
(SDRs with wired connections) and simulated RF all using srsLTE release 20.04.1. Slices are simulated by having separate sets of eNodeBs, a separate EPC with each having a different mcc/mnc.
Between one and three slices will be created. If slice 1 requires real phones it uses portable endpoint pe-test1. Slice 2 uses pe-proto1.

The following nodes will be deployed:

* One or more Intel NUC5300/B210 w/ srsLTE eNodeBs (`enb1-1`, `enb1-2`)
* Between one and three Intel NUC5300/B210 w/ srsLTE UE nodes (`rue1-1` ...`rue1-3`)
* EPC compute node(s), one per slice.
* Compute nodes acting as the NDN gateway connected to epc1 or epc2  (`cndn1` or `cndn2`)
* Shadow compute nodes connected to UE nodes (`sue1-1` ...`sue1-3`) that run Android VMs.
* One or both of the portable endpoints containing physical Android phones.
* Auxilary nodes used to provide a source for simulated DDoS data.

The different slices have their own EPCs, `epc1`, `epc2` or `epc3`. Each slice has its own MCC/MNC. Slice one uses MCC=998 and MNC=96.
Slice two uses MCC=998 and MNC=97.
`epc1` in slice 1 uses the file `user_db1.csv` for the UEs.
`epc2` in slice 2 uses the file `user_db2.csv` for the UEs.
`epc3` in slice 3 uses the file `user_db3.csv` for the UEs. These files includes real phones and UEs implemented by srsLTE.
To demonstrate the virtualization of multiple slices, by default a dedicated EPC node is created and all EPCs run as VMs on the single machine.

Instructions:

The srsLTE will be automatically run on the UE nodes (rue\*), the eNodeB nodes (enb\*) and the EPC node (epc).
The routes.sh script will be run on the NDN gateway node, cndn1, cndn2 or cndn3.
The route-mon.sh script will be run on the rue* nodes. This runs the routes-ue.sh script to ensure the
correct routes are set.

To see if the srsLTE RAN connection is established, do the following ping test on rue\* nodes. For slice 1:
```
ping 192.168.100.1
```

For slice 2:
```
ping 192.168.101.1
```

After the routes are setup, you should be able to ping from `sue*` to `cndn1` (slice 1).

```
user@sue1> ping cndn1
```

The srsLTE processes on the eNodeB and EPC and started using detached `tmux` sessions.
To interact with the processes using `tmux attach`. **Don't use ctrl-c** while attached to
the tmux session because this will terminate the process.
Instead, `ctrl-b d` will detach you
from the `tmux` session and leave it running in the background. You can reattach
with `tmux attach`.
If you are not familiar with `tmux`, it's a terminal multiplexer that
has some similarities to screen. Here's a [tmux cheat
sheet](https://tmuxcheatsheet.com).

Note that IP traffic is masqueraded when going through the UE
nodes. So, e.g., traffic from `sue1_1` will appear to come from the `rue1_1`
RAN address (192.168.100.X) at `cndn1`.  Another implication of this
masquerading is that you must generally initiate connections from the
UE side (shadow nodes).  However, port forwarding is setup for port 6363
(TCP and UDP) from the UE nodes to their associated shadow nodes.


"""

# Library imports
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN
import geni.rspec.pg as pg

class GLOBALS(object):
    UBUNTU_1804_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    SRSLTE_IMG_0 = "urn:publicid:IDN+emulab.net+image+PowderProfiles:U18LL-SRSLTE:0"
    SRSLTE_IMG_1 = "urn:publicid:IDN+emulab.net+image+PowderProfiles:U18LL-SRSLTE:1"
    SRSLTE_ZEROMQ_IMG_20 = "urn:publicid:IDN+emulab.net+image+VeriNet-5G:virtual-ue-20-4-1"
    SRSLTE_ZEROMQ_IMG_19 = "urn:publicid:IDN+emulab.net+image+VeriNet-5G:virtual-ue2"
    NUC_HWTYPE = "nuc5300"
    LAB_ENB_NODE_1 = "nuc12"
    LAB_ENB_NODE_2 = "nuc14"
    LAB_ENB_NODE_3 = "nuc13"
    LAB_ADB_NODE = "nuc1"
    COTS_UE_HWTYPE = "pixel3a-PL"
    # SRSLTE_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:U18LL-SRSLTE:0"
    PE_AGG_1 = "urn:publicid:IDN+pe-test1.powderwireless.net+authority+cm"
    PE_AGG_2 = "urn:publicid:IDN+pe-proto1.powderwireless.net+authority+cm"

def MakeRawPC(request, component_map, node_name, hardware_type, default_component = None):
    # Only use the hardware_type if the component_id is not specified.
    if component_map.has_key(node_name):
        # Component_id has been specified.
        node = request.RawPC(node_name, component_id=component_map[node_name])
        # remove the entry from the dictionary. If it's not empty at the end, generate an error.
        del component_map[node_name]
    elif default_component != None:
        # Default component has been specified
        node = request.RawPC(node_name, default_component)
    else:
        node = request.RawPC(node_name)
        node.hardware_type = hardware_type

    return node

# Get Context and Request objects.
pc = portal.Context()
request = pc.makeRequestRSpec()

# Define profile parameters
portal.context.defineParameter(
    "num_vues1",
    "Number of simulated RF srsLTE UE nodes in slice 1",
    portal.ParameterType.INTEGER, 0, None,
    "The selected number of simulated RF srsLTE UE nodes will be instantiated in slice 1. These UEs use srsLTe with ZeroMQ to connect to an eNodeB in the same node without RF."
)

portal.context.defineParameter(
    "num_vues2",
    "Number of simulated RF srsLTE UE nodes in slice 2",
    portal.ParameterType.INTEGER, 0, None,
    "The selected number of simulated RF srsLTE UE nodes will be instantiated in slice 2."
)

portal.context.defineParameter(
    "num_vues3",
    "Number of simulated RF srsLTE UE nodes in slice 3",
    portal.ParameterType.INTEGER, 0, None,
    "The selected number of simulated RF srsLTE UE nodes will be instantiated in slice 3."
)

portal.context.defineParameter(
    "portableEndpoint1",
    "Start eNodeB for Portable Endpoint pe-test1 in slice 1",
    portal.ParameterType.BOOLEAN, False,
    [True, False],
    "If True, the eNodeB for the Portable Endpoint is started in slice 1."
)

portal.context.defineParameter(
    "portableEndpoint2",
    "Start eNodeB for Portable Endpoint pe-proto1 in slice 2",
    portal.ParameterType.BOOLEAN, False,
    [True, False],
    "If True, the eNodeB for the Portable Endpoint is started in slice 2"
)

portal.context.defineParameter(
    "portableEndpoint3",
    "Start eNodeB for slice 3",
    portal.ParameterType.BOOLEAN, False,
    [True, False],
    "If True, the eNodeB is started in slice 3"
)

portal.context.defineParameter(
    "num_ues1",
    "Number of controlled RF UE nodes in slice 1",
    portal.ParameterType.INTEGER, 0, None,
    "The selected number of UE nodes will be instantiated in the controlled RF environment in slice 1.  Each will have an RF link setup to an eNodeB. No more than two total can be connected to a single eNodeB due to RF attenuator matrix path limits.",
    advanced=True
)

portal.context.defineParameter(
    "num_ues2",
    "Number of controlled RF UE nodes in slice 2",
    portal.ParameterType.INTEGER, 0, None,
    "The selected number of UE nodes will be instantiated in slice 2 in the controlled RF environment.  Each will have an RF link setup to an eNodeB. No more than two total can be connected to a single eNodeB due to RF attenuator matrix path limits.",
    advanced=True
)

portal.context.defineParameter(
    "num_ues3",
    "Number of controlled RF UE nodes in slice 3",
    portal.ParameterType.INTEGER, 0, None,
    "The selected number of UE nodes will be instantiated in slice 3 in the controlled RF environment.  Each will have an RF link setup to an eNodeB. No more than two total can be connected to a single eNodeB due to RF attenuator matrix path limits.",
    advanced=True
)

portal.context.defineParameter(
    "startSrsLte",
    "Automatically start srsLTE software",
    portal.ParameterType.BOOLEAN, True,
    [True, False],
    "If True, the scripts to start srsLTE will be run on the eNodeB, EPC and the UE. The IP routes are configured between the nodes.",
    advanced=True
)

portal.context.defineParameter(
    "EPCVM",
    "Create the EPC as a VM",
    portal.ParameterType.BOOLEAN, True,
    [True, False],
    "If True, any EPC is created as a VM. Does not apply when EPC is combined with eNodeB",
    advanced=True
)

portal.context.defineParameter(
    "VUEVM",
    "Create any simulated RF srsLTE UE as a VM",
    portal.ParameterType.BOOLEAN, True,
    [True, False],
    "If True, any simulated RF srsLTE UE is created as a VM.",
    advanced=True
)

portal.context.defineParameter(
    "num_vues_per_node",
    "Maximum number of Simulated RF srsLTE UE VMs per raw node.",
    portal.ParameterType.INTEGER, 10, None,
    "The maximum number of Simulated RF UE VMs per physical node",
    advanced=True
)

portal.context.defineParameter(
    "cnodetype",
    "Compute node type",
    portal.ParameterType.STRING, "d430",
    ["","d710","d430","d740","pc3000","d820"],
    "Type of compute nodes attached to the eNB/core and UE nodes running NDN software. Do not use pc3000 when testing NDN",
    advanced=True
)

# srsLTe is built to use AVX instructions e.g. d710 or d430.
portal.context.defineParameter(
    "epcnodetype",
    "EPC node type",
    portal.ParameterType.STRING, "d430",
    ["","d710","d430","d740"],
    "Type of EPC node attached to the eNB nodes. Use d430 if simulating a remote gateway using traffic shaping",
    advanced=True
)

portal.context.defineParameter(
    "vuenodetype",
    "Simulated RF UE node type",
    portal.ParameterType.STRING, "d430",
    ["","d710","d430","d740"],
    "Type of Simulated RF UE node",
    advanced=True
)

portal.context.defineParameter(
    "auxnode1",
    "Create an auxiliary node in slice 1",
    portal.ParameterType.BOOLEAN, False,
    [True, False],
    "If True, an auxiliary node is created for simulated DDoS traffic generation in slice 1",
    advanced=True
)

portal.context.defineParameter(
    "auxnode2",
    "Create an auxiliary node in slice 2",
    portal.ParameterType.BOOLEAN, False,
    [True, False],
    "If True, an auxiliary node is created for simulated DDoS traffic generation in slice 2",
    advanced=True
)

portal.context.defineParameter(
    "auxnode3",
    "Create an auxiliary node in slice 3",
    portal.ParameterType.BOOLEAN, False,
    [True, False],
    "If True, an auxiliary node is created for simulated DDoS traffic generation in slice 3",
    advanced=True
)

portal.context.defineParameter(
    "adb_pe_node",
    "Create an external ADB node for PEs to use",
    portal.ParameterType.BOOLEAN, False,
    [True, False],
    "If True, an additional ADB node is created for use with PEs. Not needed with simulated srsLTE is used, as an external ADB node is already created.",
    advanced=True
)

# Note: These parameters are used to simulate a remote gateway. They are NOT used for traffic shaping for DDoS simulations
portal.context.defineParameterGroup("shaping", "Remote Gateway Simulation")

# Traffic shaping can only be used on nodes without 10Gb interfaces. e.g. d430
portal.context.defineParameter(
    "simulateRemoteGateway",
    "Simulate remote gateway (cndn) using traffic shaping",
    portal.ParameterType.BOOLEAN, False,
    [True, False],
    "If True, the link between the epc node and the cndn node has traffic shaping to simulate the increased latency, lower bandwidth and possible packet loss seen with remote gateways. This shaping is NOT used for traffic shaping for DDoS simulations.",
    groupId="shaping"
)

portal.context.defineParameter(
    "packetLoss",
    "If simulate remote gateway is true, Packet loss can be specified 0.0 <= N < 1.0",
    portal.ParameterType.LOSSRATE, 0.0,
    groupId="shaping"
)

portal.context.defineParameter(
    "latency",
    "If simulate remote gateway is true, latency can be specified in ms",
    portal.ParameterType.LATENCY, 10.0,
    groupId="shaping"
)

portal.context.defineParameter(
    "bandwidth",
    "If simulate remote gateway is true, bandwidth can be specified in kbps",
    portal.ParameterType.BANDWIDTH, 10000.0,
    groupId="shaping"
)

portal.context.defineParameter(
    "explicitcomponents",
    "Optional Explicit Component Ids",
    portal.ParameterType.STRING, "",
    longDescription="Optionally specify node component ids explicitly. In the form: nodename=componentname;nodename2=componentsname2. Example: enb1=nuc1;rue1=nuc3. If not specified, Powder assigns components based on the node type and the reservations",
    advanced=True
)

portal.context.defineParameter(
    "labeNodeB1",
    "Component id for lab eNodeB in slice 1",
    portal.ParameterType.STRING, GLOBALS.LAB_ENB_NODE_1,
    longDescription="component id for eNodeB using PE",
    advanced=True
)

portal.context.defineParameter(
    "labeNodeB2",
    "Component id for lab eNodeB in slice 2",
    portal.ParameterType.STRING, GLOBALS.LAB_ENB_NODE_2,
    longDescription="component id for eNodeB using PE",
    advanced=True
)

portal.context.defineParameter(
    "labeNodeB3",
    "Component id for lab eNodeB in slice 3",
    portal.ParameterType.STRING, GLOBALS.LAB_ENB_NODE_3,
    longDescription="component id for eNodeB using PE",
    advanced=True
)

portal.context.defineParameter(
    "rfcomponents",
    "Create RF components",
    portal.ParameterType.BOOLEAN, True,
    [True, False],
    "If True, RF components will be created. Otherwise, the EPC, eNodeB(s), and RUE(s) will not be created. SUE(s) will have links directly to the CNDN, instead of through RUE(s). This allows the experiment to run when RF resources (NUC5300) are not available.",
    advanced=True
)

portal.context.defineParameter(
    "srslte_img_version",
    "srsLTE Version for eNodeB and UE in RF environments",
    portal.ParameterType.STRING, "20",
    ["19", "20"],
    "srsLTE version in RF environments. Version for UE and eNodeB",
    advanced=True
)

portal.context.defineParameter(
    "srslte_img_version_norf",
    "srsLTE Version for any simulated RF srsLTE UE environments",
    portal.ParameterType.STRING, "20",
    ["19", "20"],
    "srsLTE version for simulated RF srsLTE UE environments. Version for UE and eNodeB using ZeroMQ",
    advanced=True
)

portal.context.defineParameter(
    "srslte_img_version_epc",
    "srsLTE Version for EPC",
    portal.ParameterType.STRING, "20",
    ["19", "20"],
    "srsLTE version for EPC",
    advanced=True
)

portal.context.defineParameter(
    "vuepersue",
    "Number of Virtual UEs per SUE Node",
    portal.ParameterType.INTEGER, 1, None,
    "The number of virtual UEs to create per SUE node.",
    advanced=True
)

portal.context.defineParameter(
    "enbPcap",
    "Perform pcap recording enb",
    portal.ParameterType.BOOLEAN, False,
    [True, False],
    "If True, the eNodeB will record in /tmp",
    advanced=True
)

portal.context.defineParameter(
    "fakeSdr",
    "Create an empty node (for testing only)",
    portal.ParameterType.BOOLEAN, False,
    [True, False],
    "If True, Just create an empty eNodeB node for testing",
    advanced=True
)


# Bind parameters
params = portal.context.bindParameters()

# srsLTE Image for RF environments
if params.srslte_img_version == "20":
    srslte_img = GLOBALS.SRSLTE_IMG_1
else:
    srslte_img = GLOBALS.SRSLTE_IMG_0

# srsLTE Image for simulated RF environments using ZeroMQ
if params.srslte_img_version_norf == "20":
    srslte_img_vue = GLOBALS.SRSLTE_ZEROMQ_IMG_20
else:
    srslte_img_vue = GLOBALS.SRSLTE_ZEROMQ_IMG_19

# srsLTE Image for EPC
if params.srslte_img_version_epc == "20":
    srslte_img_epc = GLOBALS.SRSLTE_IMG_1
else:
    srslte_img_epc = GLOBALS.SRSLTE_IMG_0

component_map = {}
if len(params.explicitcomponents) > 0:
    for kv in params.explicitcomponents.split(";"):
      kvlist = kv.split('=')
      if len(kvlist) != 2:
        portal.context.reportError(portal.ParameterError( "Explicit Components value is invalid %s" % kv,
            ['explicitcomponents'] ),
                immediate=True )
      component_map[kvlist[0].strip()] = kvlist[1].strip()

class UE:
    rf_controlled = False

class Slice:
    def __init__(self, slice_number, num_ues, num_vues, portable_endpoint, auxnode):
        self.ues = []
        self.slice_number = slice_number
        self.num_ues = num_ues
        self.num_vues = num_vues
        self.portable_endpoint = portable_endpoint
        self.auxnode = auxnode

if params.num_ues1 < 0 or params.num_ues2 < 0 or params.num_ues3 < 0:
  portal.context.reportError(portal.ParameterError( "Number of UEs cannot be negative",
      ['num_ues1', 'num_ues2'] ),
          immediate=True )

if params.num_vues1 < 0 or params.num_vues2 < 0 or params.num_vues3 < 0:
  portal.context.reportError(portal.ParameterError( "Number of simulated RF UEs cannot be negative",
      ['num_vues1', 'num_vues2'] ),
          immediate=True )

if params.vuepersue <= 0:
  portal.context.reportError(portal.ParameterError( "Number of virtual UEs per sue must be positive",
      ['vuepersue'] ),
          immediate=True )

# total UEs in controlled RF environment
totalUEs = params.num_ues1 + params.num_ues2 + params.num_ues3

if params.rfcomponents and totalUEs > 3:
  portal.context.reportError(portal.ParameterError( "Too many UEs specified for controlled RF environment",
      ['num_ues1'] ),
          immediate=True )

if params.portableEndpoint1 and params.portableEndpoint2 and params.labeNodeB1 == params.labeNodeB2 and not params.fakeSdr:
  portal.context.reportError(portal.ParameterError( "eNodeB component ids must be different",
      ['labeNodeB1', 'labeNodeB2'] ),
          immediate=True )

if params.portableEndpoint2 and params.portableEndpoint3 and params.labeNodeB2 == params.labeNodeB3 and not params.fakeSdr:
  portal.context.reportError(portal.ParameterError( "eNodeB component ids must be different",
      ['labeNodeB2', 'labeNodeB3'] ),
          immediate=True )

slices = []
if params.num_ues1 > 0 or params.num_vues1 > 0 or params.portableEndpoint1 or params.auxnode1:
    slices.append(Slice(1, params.num_ues1, params.num_vues1, params.portableEndpoint1, params.auxnode1))
if params.num_ues2 > 0 or params.num_vues2 > 0 or params.portableEndpoint2 or params.auxnode2:
    slices.append(Slice(2, params.num_ues2, params.num_vues2, params.portableEndpoint2, params.auxnode2))
if params.num_ues3 > 0 or params.num_vues3 > 0 or params.portableEndpoint3 or params.auxnode3:
    slices.append(Slice(3, params.num_ues3, params.num_vues3, params.portableEndpoint3, params.auxnode3))

# This is total number of controlled RF eNodeB's used for allocating frequencies so is not within a slice.
globalEnodebNumber=1

androidVMStartNumber = 1

# Add ADB node
needadb = params.num_vues1 > 0 or params.num_vues2 > 0 or params.num_ues1 > 0 or params.num_ues2 > 0 or params.adb_pe_node 
if needadb:
  adb = MakeRawPC(request, component_map, "adb", "")
  adb.disk_image = GLOBALS.UBUNTU_1804_IMG
  adb.addService(rspec.Execute(shell="bash", command="/local/repository/install.sh"))
pcap = ""
if params.enbPcap:
    pcap = "pcap"

if params.EPCVM:
    # Raw node to instantiate EPC VMs on
    epc_node = request.RawPC('epcnode')
    epc_node.hardware_type = params.epcnodetype

for slice_item in range(1, len(slices) + 1):
  slice = slices[slice_item -1]
  sliceNumber = slice.slice_number
  vues_vm_count = 10000
  vues_node_count = 0
  epcpeers = []

  # The tunnel IP address depends on the EPC
  # The default subnet 172.16/12 is not used because this subnet is used by Powder for VMs.
  if sliceNumber == 1:
      tun_ip = "192.168.100.1"
      rf_subnet = "192.168.100.0/24"
  else:
      if sliceNumber == 2:
          tun_ip = "192.168.101.1"
          rf_subnet = "192.168.101.0/24"
      else:
          tun_ip = "192.168.102.1"
          rf_subnet = "192.168.102.0/24"

  if params.rfcomponents:
    lan = request.LAN(name = "epc%d-link" % sliceNumber)
    # Turn on link multiplexing. Note that this also turns on vlan encapsulation
    lan.link_multiplexing = True

    # But the resource mapper is going to try to prevent the two links from oversubscribing
    # the physical link. For example, trying to create two 1Gb multiplexed links on top of a 1Gb
    # physical link. Sometimes this is the correct behaviour. But if not, do this to turn
    # off the checks.
    lan.best_effort = True

    epcNodeName = "epc%d" % sliceNumber
    num_enbs = (slice.num_ues+1)//2
    enbs = []

    for enb_num in range(1,num_enbs + 1):
      # Add a NUC eNB node in controlled RF environment
      enb = MakeRawPC(request, component_map, "enb%d-%d" % (sliceNumber, enb_num),  GLOBALS.NUC_HWTYPE)
      enb.disk_image = srslte_img
      enb.Desire("rf-controlled", 1)
      enb.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))
      enb.addService(rspec.Execute(shell="bash", command="/local/repository/add-nat-and-ip-forwarding.sh"))
      if params.startSrsLte:
        enb.addService(rspec.Execute(shell="bash", command="/local/repository/start-enb-impl.sh %d %s %d %s x %s" % (sliceNumber, epcNodeName, globalEnodebNumber, params.srslte_img_version, pcap)))
      enb_link = enb.addInterface("epcif")
      lan.addInterface(enb_link)
      enbs.append(enb)
      globalEnodebNumber = globalEnodebNumber + 1

    assert len(enbs) == num_enbs

    if slice.portable_endpoint:

      # Assume a single PE per slice for now.
      # Add ADB node on PE only for pe-test1.
      if not params.fakeSdr:
        if sliceNumber == 1:
          peadb = MakeRawPC(request, component_map, "adb%d" % sliceNumber, params.cnodetype, default_component = GLOBALS.LAB_ADB_NODE)

          peadb.component_manager_id = GLOBALS.PE_AGG_1
          peadb.disk_image = GLOBALS.UBUNTU_1804_IMG
          peadb.addService(rspec.Execute(shell="bash", command="/local/repository/install.sh"))

      # Add eNodeB for portable endpoint
      enb_num = num_enbs + 1;
      if params.fakeSdr:
        # Just for testing topology and does not run any srsLTE code.
        enb = MakeRawPC(request, component_map, "enb%d-%d" % (sliceNumber, enb_num),  params.cnodetype)
        enb.disk_image = GLOBALS.UBUNTU_1804_IMG
      else:
        enb = MakeRawPC(request, component_map, "enb%d-%d" % (sliceNumber, enb_num),  GLOBALS.NUC_HWTYPE)
        if sliceNumber == 1:
          enb.component_id = params.labeNodeB1
        elif sliceNumber == 2:
          enb.component_id = params.labeNodeB2
        else:
          enb.component_id = params.labeNodeB3
        enb.Desire("rf-radiated", 1)
        enb.disk_image = srslte_img
        enb.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))
        enb.addService(rspec.Execute(shell="bash", command="/local/repository/add-nat-and-ip-forwarding.sh"))
        if params.startSrsLte:
          enb.addService(rspec.Execute(shell="bash", command="/local/repository/start-enb-impl.sh %d %s %d %s pe %s" % (sliceNumber, epcNodeName, globalEnodebNumber, params.srslte_img_version, pcap)))
      enb_link = enb.addInterface("epcif")
      lan.addInterface(enb_link)

    # Add EPC server(s), core network side
    if params.EPCVM:
      epc = request.XenVM(epcNodeName, exclusive=True)
      epc.routable_control_ip = True
      epc.cores = 8
      epc.ram = 2048
      epc.InstantiateOn(epc_node)
      epc.Attribute('XEN_FORCE_HVM','1')
      # if params.epcnodetype != "":
        # epc.xen_ptype = params.epcnodetype + "-vm"
    else:
      epc = MakeRawPC(request, component_map, epcNodeName,  params.epcnodetype)

    epc.disk_image = srslte_img_epc
    epc.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))
    epc.addService(rspec.Execute(shell="bash", command="/local/repository/add-nat-and-ip-forwarding.sh"))
    epc.addService(rspec.Execute(shell="bash", command="/local/repository/install.sh"))
    if params.startSrsLte:
      epc.addService(rspec.Execute(shell="bash", command="/local/repository/start-epc-impl.sh %d %s" % (sliceNumber, tun_ip)))
      epc.addService(rspec.Execute(shell="bash", command="/local/repository/routes.sh"))

    epc_link = epc.addInterface("enbif")
    lan.addInterface(epc_link)


  # Add NDN server, core network side
  cndnnode = "cndn%d" % sliceNumber
  cndn = MakeRawPC(request, component_map, cndnnode, params.cnodetype)
  cndn.disk_image = GLOBALS.UBUNTU_1804_IMG
  cndn.addService(rspec.Execute(shell="bash", command="/local/repository/install.sh"))
  if needadb:
    adb_link = request.Link("adb-link-%d" % sliceNumber, members=[cndn, adb])
    adb_link.link_multiplexing = True
    adb_link.best_effort = True

  if params.rfcomponents:
    epcpeers.append(cndn)
    if slice.auxnode:
      auxnode = "aux%d" % sliceNumber
      aux = MakeRawPC(request, component_map, auxnode, params.cnodetype)
      aux.disk_image = GLOBALS.UBUNTU_1804_IMG
      epcpeers.append(aux)
      aux.addService(rspec.Execute(shell="bash", command="/local/repository/install.sh"))
      aux.addService(rspec.Execute(shell="bash", command="/local/repository/routes-cndn.sh %d %s %s" % (sliceNumber, epcNodeName, rf_subnet)))

    epcpeers.append(epc)
    cndn_link = request.Link("cndn%d-link" % sliceNumber, members=epcpeers)
    cndn_link.link_multiplexing = True
    cndn_link.best_effort = True
    if params.simulateRemoteGateway:
      # Bandwidth is in kbps
      cndn_link.bandwidth = params.bandwidth
      # Latency is in ms
      cndn_link.latency = params.latency
      # Packet loss is a number 0.0 <= loss <= 1.0
      cndn_link.plr = params.packetLoss
    if params.startSrsLte:
      cndn.addService(rspec.Execute(shell="bash", command="/local/repository/routes-cndn.sh %d %s %s" % (sliceNumber, epcNodeName, rf_subnet)))

  # We need a link to talk to the remote file system, so make an interface.
  cndn_iface = cndn.addInterface()

  # The remote file system is represented by special node.
  cndn_fsnode = request.RemoteBlockstore("cndn%d-fsnode" % sliceNumber, "/mydata")
  # This URN is displayed in the web interfaace for your dataset.
  # VM Dataset
  cndn_fsnode.dataset = "urn:publicid:IDN+emulab.net:verinet-5g+ltdataset+nfd-gw"
  #
  # The "rwclone" attribute allows you to map a writable copy of the
  # indicated SAN-based dataset. In this way, multiple nodes can map
  # the same dataset simultaneously. In many situations, this is more
  # useful than a "readonly" mapping. For example, a dataset
  # containing a Linux source tree could be mapped into multiple
  # nodes, each of which could do its own independent,
  # non-conflicting configure and build in their respective copies.
  # Currently, rwclones are "ephemeral" in that any changes made are
  # lost when the experiment mapping the clone is terminated.
  #
  cndn_fsnode.rwclone = True

  #
  # The "readonly" attribute, like the rwclone attribute, allows you to
  # map a dataset onto multiple nodes simultaneously. But with readonly,
  # those mappings will only allow read access (duh!) and any filesystem
  # (/mydata in this example) will thus be mounted read-only. Currently,
  # readonly mappings are implemented as clones that are exported
  # allowing just read access, so there are minimal efficiency reasons to
  # use a readonly mapping rather than a clone. The main reason to use a
  # readonly mapping is to avoid a situation in which you forget that
  # changes to a clone dataset are ephemeral, and then lose some
  # important changes when you terminate the experiment.
  #
  #cndn_fsnode.readonly = True

  # Now we add the link between the node and the special node
  cndn_fslink = request.Link("cndn%d-fslink" % sliceNumber)
  cndn_fslink.addInterface(cndn_iface)
  cndn_fslink.addInterface(cndn_fsnode.interface)

  # Special attributes for this link that we must use.
  cndn_fslink.best_effort = True
  cndn_fslink.vlan_tagging = True

  # Add the requested number of UE nodes (attached to the eNodeB via
  # controlled RF links or via ZeroMQ without RF).  Each has its own shadow compute node.
  total_ue = slice.num_ues + slice.num_vues
  vue_count = 0
  sue_id = 0
  suepeers = []
  for id in range(1, total_ue+ 1):
      if id > slice.num_ues:
          # Make a simulated RF UE which includes an eNodeB connected via ZeroMQ
          ue_name = "rue%d-%d" % (sliceNumber,id)
          if params.VUEVM:
            if vues_vm_count >= params.num_vues_per_node:
              vues_node_count = vues_node_count + 1
              rue_node = request.RawPC('ruenode%d-%d' % (sliceNumber, vues_node_count))
              rue_node.hardware_type = params.vuenodetype
              vues_vm_count = 0
            vues_vm_count = vues_vm_count + 1
            rue = request.XenVM(ue_name, exclusive=True)
            rue.cores = 8
            rue.ram = 5 * 1024
            rue.Attribute('XEN_FORCE_HVM','1')
            rue.InstantiateOn(rue_node)
            # if params.vuenodetype != "":
              # rue.xen_ptype = params.vuenodetype + "-vm"
          else:
            rue = MakeRawPC(request, component_map, ue_name, params.vuenodetype)
          rue.disk_image = srslte_img_vue
          rue.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))
          rue.addService(rspec.Execute(shell="bash", command="/local/repository/install.sh"))
          if params.startSrsLte:
            rue.addService(rspec.Execute(shell="bash", command="/local/repository/ping-test.sh %s" % tun_ip))
            rue.addService(rspec.Execute(shell="bash", command="/local/repository/start-ue-impl.sh %d %d norf" % (sliceNumber, id)))
            rue.addService(rspec.Execute(shell="bash", command="/local/repository/start-enb-impl.sh %d %s %d %s norf" % (sliceNumber, epcNodeName, globalEnodebNumber, params.srslte_img_version_norf)))
            rue.addService(rspec.Execute(shell="bash", command="/local/repository/routes-mon.sh %s %s-%s-link" % (tun_ip, cndnnode, cndnnode)))
          rue_link = rue.addInterface("epcif")
          lan.addInterface(rue_link)
          suepeers.append(rue)
      elif params.rfcomponents:
          # Add a NUC node for the UE
          rue = MakeRawPC(request, component_map, "rue%d-%d" % (sliceNumber,id), GLOBALS.NUC_HWTYPE)
          rue.disk_image = srslte_img
          rue.Desire("rf-controlled", 1)
          rue.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))
          rue.addService(rspec.Execute(shell="bash", command="/local/repository/install.sh"))

          if id < 3:
              enb_num = 1
          else:
              enb_num = 2
          assert enb_num <= num_enbs
          enb = enbs[enb_num-1]

          if params.startSrsLte:
            rue.addService(rspec.Execute(shell="bash", command="/local/repository/ping-test.sh %s" % tun_ip))
            rue.addService(rspec.Execute(shell="bash", command="/local/repository/start-ue-impl.sh %d %d" % (sliceNumber, id)))
            rue.addService(rspec.Execute(shell="bash", command="/local/repository/routes-mon.sh %s %s-%s-link" % (tun_ip, cndnnode, cndnnode)))

          # Create the RF link between this UE and the eNodeB
          rflink = request.RFLink("rflink%d-%d" % (sliceNumber, id))
          enb_rf_if = enb.addInterface("enb%d-%d_rf_if%d" % (sliceNumber, enb_num, id))
          rue_rf_if = rue.addInterface("rue%d-%d_rf_if" % (sliceNumber, id))
          rflink.addInterface(enb_rf_if)
          rflink.addInterface(rue_rf_if)
          suepeers.append(rue)

      vue_count = vue_count + 1
      # Add UE shadow node
      if vue_count >= params.vuepersue or id == total_ue:
          vue_count = 0
          sue_id = sue_id + 1

          sue = MakeRawPC(request, component_map, "sue%d-%d" % (sliceNumber, sue_id), params.cnodetype)
          sue.disk_image = GLOBALS.UBUNTU_1804_IMG
          sue.addService(rspec.Execute(shell="bash", command="/local/repository/install.sh %d %d %d" % (sliceNumber, params.vuepersue, androidVMStartNumber)))
          androidVMStartNumber = androidVMStartNumber + 1

          if len(suepeers) == 0:
              # When there's no rue, connect directly to cndn
              suepeers.append(cndn)
          suepeers.append(sue)
          if needadb:
              suepeers.append(adb)
          sue_link = request.Link("sue%d-%d-link" % (sliceNumber, sue_id), members=suepeers)
          sue_link.link_multiplexing = True
          sue_link.best_effort = True
          suepeers=[]

          # We need a link to talk to the remote file system, so make an interface.
          sue_iface = sue.addInterface()

          # The remote file system is represented by special node.
          sue_fsnode = request.RemoteBlockstore("sue%d-%d-fsnode" % (sliceNumber, sue_id), "/mydata")
          # This URN is displayed in the web interface for your dataset.
          # VM Dataset
          sue_fsnode.dataset = "urn:publicid:IDN+emulab.net:verinet-5g+ltdataset+virtual-machines"
          #
          # The "rwclone" attribute allows you to map a writable copy of the
          # indicated SAN-based dataset. In this way, multiple nodes can map
          # the same dataset simultaneously. In many situations, this is more
          # useful than a "readonly" mapping. For example, a dataset
          # containing a Linux source tree could be mapped into multiple
          # nodes, each of which could do its own independent,
          # non-conflicting configure and build in their respective copies.
          # Currently, rwclones are "ephemeral" in that any changes made are
          # lost when the experiment mapping the clone is terminated.
          #
          sue_fsnode.rwclone = True

          #
          # The "readonly" attribute, like the rwclone attribute, allows you to
          # map a dataset onto multiple nodes simultaneously. But with readonly,
          # those mappings will only allow read access (duh!) and any filesystem
          # (/mydata in this example) will thus be mounted read-only. Currently,
          # readonly mappings are implemented as clones that are exported
          # allowing just read access, so there are minimal efficiency reasons to
          # use a readonly mapping rather than a clone. The main reason to use a
          # readonly mapping is to avoid a situation in which you forget that
          # changes to a clone dataset are ephemeral, and then lose some
          # important changes when you terminate the experiment.
          #
          #sue_fsnode.readonly = True

          # Now we add the link between the node and the special node
          sue_fslink = request.Link("sue%d-%d-fslink" % (sliceNumber, sue_id))
          sue_fslink.addInterface(sue_iface)
          sue_fslink.addInterface(sue_fsnode.interface)

          # Special attributes for this link that we must use.
          sue_fslink.best_effort = True
          sue_fslink.vlan_tagging = True


# if the map is not empty, at least one of the mappings was not used. In this case, generate an error.
if len(component_map) > 0:
    for key in component_map:
       portal.context.reportError(portal.ParameterError( "Explicit Component Id specified for unknown node %s" % key,
           ['explicitcomponents'] ),
               immediate=True )

pc.printRequestRSpec(request)
