#!/bin/bash
# Use this on the EPC node to limit the bandwidth on the srsLTE tunnel.
# Parameter 1: bandwidth e.g. 40000k for 40Mb/s. Default is 50000K

# This is used as part of a simulation of a DDoS using multiple parallel streams
# to overload the network.
# With a simple Token Bucket Filter (TBF), the bandwidth can be limited e.g. to
# 50Mb/s. However, this filter is not fair and is unrealistic because single UDP
# iperf3 command with a high bandwidth can be used to choke off all downstream
# data. So Stochastic Fairness Queueing (SFQ) was added to make it less unfair
# and more realistic. The single iperf3 command now shares the bandwidth with
# other flows (unless there is a hash collision which has a probably of 1/1024
# with rehashing every 10 seconds). To defeat this SFQ we can use multiple
# parallel streams in iperf3 (-P parameter) because each will be typically in
# a different hash bucket. The iperf3 session can now utilize as much bandwidth
# as it wants. We should assume a commercial network has more sophisticated
# controls and wouldn’t allow a simple bandwidth overload like this to let one
# user to take all the bandwidth.

set -x
sudo tc qdisc delete dev srs_spgw_sgi root
sudo tc qdisc add dev srs_spgw_sgi root handle 1: tbf rate ${1:-50000k}bit latency 20ms burst 20000
sudo tc qdisc add dev srs_spgw_sgi parent 1: handle 10: sfq perturb 10

