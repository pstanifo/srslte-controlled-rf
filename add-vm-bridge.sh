#!/bin/bash

# To be run on the `sue` compute nodes in the "scenario 2" profile.
# This sets up the network bridge `br0` on the `sue` for virtual machines
# to connect to using the network interface that can reach the `rue`.

NODE_ID=$(geni-get client_id)
SNODE_ID="${NODE_ID}-${NODE_ID}-link"
SNODE_RUE_IPADDR=$(getent hosts $SNODE_ID | cut -s -d ' ' -f 1)
SNODE_RUE_INTF=$(ip a | grep $SNODE_RUE_IPADDR | awk -F ' ' '{print $NF}')
SNODE_RUE_NET_MASK=$(ifconfig $SNODE_RUE_INTF | awk '/netmask/{ print $4;}')
ERR=0

sudo brctl addbr br0 || ERR=1
sudo brctl addif br0 $SNODE_RUE_INTF || ERR=1
sudo ifconfig $SNODE_RUE_INTF 0.0.0.0 || ERR=1
sudo ifconfig br0 $SNODE_RUE_IPADDR netmask $SNODE_RUE_NET_MASK up || ERR=1


if [[ $ERR == 1 ]]; then
    echo "Failed to add network bridge br0 for virtual machines!"
else
    echo "Finished adding network bridge br0 for virtual machines."
fi

exit $ERR
