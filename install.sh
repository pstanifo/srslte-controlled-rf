#!/bin/bash

# Run appropriate install scripts

MYPATH=$(realpath $0 | xargs dirname)
PROJECT_PATH="/proj/*/"
INSTALL_PATH="${MYPATH}/install-scripts/"
DATASET_PATH="/mydata/configure-scripts/"
NODE_ID=$(geni-get client_id)
SLICE=$1
VUE_PER_SUE=$2 # Number of virtual UEs per SUE node
VM_START_NUMBER=$3


function runonce {
    FILENAME=/var/tmp/runonce_`echo $1 | sed 's=\/=__=g'`
    if [ -f $FILENAME ]; then
	# The file exists so the command has already been run
	# during a previous boot.
	echo $FILENAME exists. Not running $1
	return
    fi
    $@
    # The file is created after the command has been run successfully
    > $FILENAME
}


(
runonce ${INSTALL_PATH}/update-apt-get.sh
runonce ${INSTALL_PATH}/install-iperf3.sh

if [[ $NODE_ID == "sue"* ]]; then
    ${MYPATH}/add-vm-bridge.sh
    runonce ${INSTALL_PATH}/install-kvm.sh
    runonce ${INSTALL_PATH}/install-udisks2.sh
    ${DATASET_PATH}/configure.sh ${SLICE} ${VUE_PER_SUE} ${VM_START_NUMBER}
elif [[ $NODE_ID == "cndn"* ]]; then
    runonce ${INSTALL_PATH}/install-runc.sh
    ${DATASET_PATH}/configure.sh
elif [[ $NODE_ID == "adb"* ]]; then
    runonce ${INSTALL_PATH}/install-adb.sh
    runonce ${INSTALL_PATH}/install-vnc.sh
    ${PROJECT_PATH}/configure-scripts/adb/configure.sh
else
    echo "No packages to install on $NODE_ID"
    exit 0
fi

# Somewhere in the configure scripts the log file ownership is changed to root which is wrong 
# and prevents this script from running again. So hide the log file with a cat command.
) |& cat &> /tmp/install-script.log
