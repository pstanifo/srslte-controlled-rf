#!/bin/bash

# Parameter 1: EPC number or Slice Number
# Parameter 2: EPC Node Name
# Parameter 3: Controlled RF eNodeB instance number not within the slice. This is used to allocate frequencies.
# Parameter 4: the major version number of srsLTE (19 or 20)
# Parameter 5: optionally "pe" for eNodeB OTA using Portable Endpoint. "norf" for ZeroMQ (Simulated/ No RF)
# Parameter 6: optionally "pcap" for recording in pcap file

MYPATH=$(realpath $0 | xargs dirname)
# First decide which EPC to use. 
HOSTNAME=$(hostname | cut -s -d '.' -f 1)
EPC=$2

# IP address of the epc node interface towards the enb node(s)
EPC_ENB_IPADDR=$(getent hosts $EPC-$EPC-link | cut -s -d ' ' -f 1)

# IP address of the interface towards the epc node
ENB_EPC_IPADDR=$(getent hosts $HOSTNAME-$EPC-link| cut -s -d ' ' -f 1)
ENODEB_NUM=$3
ENB_PARAM=""
    
# each eNodeB has different frequencies and IDs.
if [ $5 == "pe" ]; then
   ENB_PARAM="--rf.tx_gain 80 --rf.rx_gain 50"
   if [ $1 == 1 ]; then
	# Slice 1
	# Set the DL EARFCN which specifies the DL frequency. The matching UL frequency is automatically selected.
	DL_EARFCN=3400
	ENODEB_NUM=0
	MNC=96
   else
        if [ $1 == 2 ]; then
	      # Slice 2
	      # Set the DL EARFCN which specifies the DL frequency. The matching UL frequency is automatically selected.
	      DL_EARFCN=2800
	      ENODEB_NUM=1
	      MNC=97
	 else 
	      # Slice 3
	      # Set the DL EARFCN which specifies the DL frequency. The matching UL frequency is automatically selected.
	      DL_EARFCN=3100
	      ENODEB_NUM=2
	      MNC=98
	 fi
   fi
else
  if [ $ENODEB_NUM == 1 ]; then
	# Set the DL EARFCN which specifies the DL frequency. The matching UL frequency is automatically selected.
	DL_EARFCN=3400
  else
        if [ $ENODEB_NUM == 2 ]; then
	    # Set the DL EARFCN which specifies the DL frequency. The matching UL frequency is automatically selected.
	    DL_EARFCN=2800
	else
	    # Set the DL EARFCN which specifies the DL frequency. The matching UL frequency is automatically selected.
	    DL_EARFCN=3100
        fi
  fi
  if [ $1 == 1 ]; then
	# Slice 1
	MNC=96
  else
        if [ $1 == 2 ]; then
	    # Slice 2
	    MNC=97
	else
	    # Slice 3
	    MNC=98
        fi
  fi
fi
((ID=0x19A+$ENODEB_NUM))
ENB_ID=$(printf "0x%03X" $ID)

# Create a script file to start the eNodeB. Because of reliability problems with srsLTE, this script:
#  1) waits until the EPC node is up before starting the eNodeB process.
#  2) Restarts the eNodeB process if it terminates
#
# This script is also used by the restart scripts (restart-enb.sh and restart-enbue.sh)
# A lock file is created which is deleted whenever the restart script is run in case a different user performs the restart.
# Deleting the lock file, makes the restart loop stop.
SCRIPTFILE=/local/repository/start-enb-in.sh
sudo rm $SCRIPTFILE
(
echo "# Created by $0 $@"
echo
echo "# If srsenb terminates, restart it after 2 second"
echo '> /tmp/enb.lock.$$'
echo "while [ 1 ]; do"
echo "  # srsenb frequently crashes if it can't access the EPC so wait until we can ping it"
echo "  /local/repository/waitup.sh $EPC_ENB_IPADDR"
echo "  DATETIME=\`date +%y%m%d%H%M\`"
PARAMS=""
if [ $4 == "19" ]; then
    # srsLTE release 19
    # This parameter has been removed in version 20
    PARAMS="$PARAMS --enb.tac 0x0001"
else
    # srsLTE release 20
    # Make RRC inactivity timer equal to 10 hours (36,000 seconds) instead of one minute. This hides the stability problem
    # in release 20 whenever the UE goes between IDLE and CONNECTED states.
    PARAMS="$PARAMS  --expert.rrc_inactivity_timer 36000000"
fi
if [ "$6" == "pcap" ]; then
    PARAMS="$PARAMS --pcap.enable=true"
fi
if [ $5 == "norf" ]; then
    # For the simulated RF - ZeroMQ environment. 
    PARAMS="$PARAMS --rf.device_name=zmq --rf.device_args=\"fail_on_disconnect=true,tx_port=tcp://*:2000,rx_port=tcp://localhost:2001,id=enb,base_srate=23.04e6\""
    PARAMS="$PARAMS --enb_files.sib_config /local/repository/sib.conf.zeromq"
fi
echo "  sudo srsenb /etc/srslte/enb.conf --enb.enb_id=$ENB_ID --rf.dl_earfcn $DL_EARFCN --enb.mme_addr $EPC_ENB_IPADDR --enb.gtp_bind_addr $ENB_EPC_IPADDR --enb.s1c_bind_addr $ENB_EPC_IPADDR --enb.mcc=998 --enb.mnc=$MNC $ENB_PARAM --log.filename=/tmp/enb."'$DATETIME'".log $PARAMS"
echo "  echo enb terminated"
echo "  date"
echo "  sleep 2"
echo "  # The restart script(s) delete this file"
echo '  if [ ! -f /tmp/enb.lock.$$ ]; then'
echo "     echo Lock file deleted. Sleeping"
echo "     break"
echo "  fi"
echo "done"
)> $SCRIPTFILE
chmod +x $SCRIPTFILE

# Delete any existing tmux session for srsenb started by this user.
tmux kill-session -t enb
tmux new-session -d -s enb "bash $SCRIPTFILE; sleep 300"
