#!/bin/bash
# Parameter 1: Slice
# Parameter 2: UE Number (1,2,3)
# Parameter 3: norf if using ZeroMQ

MYPATH=$(realpath $0 | xargs dirname)
NODE_ID=$(geni-get client_id)
NUM=${NODE_ID: -1}
SLICE=$(printf "%01x" $1)
HEXID=$(printf "%02x" $2)
if [ $1 == 1 ]; then
	MNC=96
else
    if [ $1 == 2 ]; then
	MNC=97
    else
	MNC=98
    fi
fi
SCRIPTFILE=/local/repository/start-ue-in.sh
COMMAND="nohup sudo srsue /etc/srslte/ue.conf --rf.dl_earfcn=2800,3400,3100 --usim.imsi=998${MNC}0123456$SLICE$HEXID"
if [ $3 == "norf" ]; then
  COMMAND="$COMMAND --rf.device_name=zmq --rf.device_args=\"tx_port=tcp://*:2001,rx_port=tcp://localhost:2000,id=ue,base_srate=23.04e6\""
fi
(
echo "# Created by $0 $@"
echo
echo "DATETIME=\`date +%y%m%d%H%M\`"
echo "$COMMAND --log.filename=/tmp/ue."'$DATETIME'.log
)> $SCRIPTFILE
chmod +x $SCRIPTFILE

echo "$COMMAND" | $MYPATH/log.sh
bash $SCRIPTFILE&
