#!/usr/bin/env bash

# To be run on the `rue` compute nodes in the "scenario 2" profile.
# This sets up masquerading of IP traffic from downstream (`nodea`),
# and adds a route pushing most 10.0.0.0/8 traffic to the RAN uplink.
#
# Argument 1: Tunnel IP address e.g. 172.16.0.1
# Argument 2: cndn node name

MYPATH=$(realpath $0 | xargs dirname)
UE_RAN_DEV="tun_srsue"
RTR_IP=$1
NODE_ID=$(geni-get client_id)
CNDN_IPADDR=$(getent hosts $2 | cut -s -d ' ' -f 1)
ERR=0

# Make sure there are no routes to the subnet containing cndn node. 
# Delete any containing this subnet and make the subnet unreachable.
# Note that the /32 subnet we add is not deleted.
SUBNET_PREFIX=`getent hosts $2 | cut -f1 -d\  | sed 's/\.[0-9]*$//'`
# Delete route(s). These may not exist at all because sometimes Powder doesn't add explicit routes on this subnet.
ip route | grep "^$SUBNET_PREFIX.[0-9]*/" | cut -d\  -f 1 | xargs -i sudo ip route delete {}
# Force this /24 subnet used by cndn node to be unreachable. This stops traffic to cndn 
# from going over regular ethernet when the LTE tunnel does not exist.
ip route add unreachable $SUBNET_PREFIX.0/24

# Wait until UE RAN Device is up before adding ip routes and iptables rules
if [[ $(ip a | grep -c $UE_RAN_DEV) > 0 ]]; then
	ip route add 10.0.0.0/8 via $RTR_IP || ERR=1
	# for cndn and aux node (assuming there're at the low end of the subnet)
	ip route add $SUBNET_PREFIX/28 via $RTR_IP || ERR=1
	# Only modify iptables if it nat does not exist already
	iptables -t nat -S | grep "\-o tun_srsue" > /dev/null
        if [ $? == 1 ]; then
	  # Flush before add
	  iptables -t nat -F || ERR=1
	  iptables -t nat -A POSTROUTING -o $UE_RAN_DEV -j MASQUERADE || ERR=1
	fi
else
	ERR=1
	echo $(basename $0) "${UE_RAN_DEV} is not up yet!"
fi | $MYPATH/log.sh

echo "RTR_IP = ${RTR_IP}" | $MYPATH/log.sh

if [[ $ERR == 1 ]]; then
	echo $(basename $0) "Failed to add routes!"
else
	echo $(basename $0) "Finished adding routes."
fi | $MYPATH/log.sh

exit $ERR
