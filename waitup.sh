#!/bin/bash                                                                                                                         
# Keeps pinging until $1 responds to ping
code=1                                                                                                                              
while [ $code == 1 ]; do                                                                                                            
  ping -c 1 -t 1 $1                                                                                                                 
  code=$?                                                                                                                           
  sleep 2                                                                                                                           
done     
