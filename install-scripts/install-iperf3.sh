#!/bin/bash

MYPATH=$(realpath $0 | xargs dirname)

sudo apt -y remove iperf3 iperf libiperf0
sudo apt -y install libsctp1

sudo wget https://iperf.fr/download/ubuntu/libiperf0_3.7-3_amd64.deb
sudo wget https://iperf.fr/download/ubuntu/iperf3_3.7-3_amd64.deb

sudo dpkg -i libiperf0_3.7-3_amd64.deb iperf3_3.7-3_amd64.deb

rm libiperf0_3.7-3_amd64.deb iperf3_3.7-3_amd64.deb
