#!/bin/bash

# Parameter 1: Slice Number
# Parameter 2: EPC Tunnel IP address e.g. 172.16.0.1

MYPATH=$(realpath $0 | xargs dirname)
NODE_ID=$(geni-get client_id)
# bind to the interface towards the enb node(s)
EPC_ENB_IPADDR=$(getent hosts ${NODE_ID}-${NODE_ID}-link | cut -s -d ' ' -f 1)
SLICE_NUM=$1
RTR_IP=$2
if [ $1 == 1 ]; then
  MNC=96
else
  if [ $1 == 2 ]; then
     MNC=97
  else
     MNC=98
  fi
fi

SCRIPTFILE=/local/repository/start-epc-in.sh
sudo rm $SCRIPTFILE
(
echo "# Created by $0 $@"
echo
echo '> /tmp/epc.lock.$$'
echo "# If srsepc terminates, restart it after 2 second"
echo "while [ 1 ]; do"
echo "  DATETIME=\`date +%y%m%d%H%M\`"
echo "  sudo srsepc /etc/srslte/epc.conf --mme.mme_bind_addr $EPC_ENB_IPADDR --spgw.gtpu_bind_addr $EPC_ENB_IPADDR --spgw.sgi_if_addr $RTR_IP --mme.mcc 998 --mme.mnc $MNC --mme.tac 0x0001 --hss.db_file=/local/repository/user_db$SLICE_NUM.csv --log.filename=/tmp/epc."'$DATETIME'".log"
echo "  echo epc terminated"
echo "  date"
echo "  sleep 2"
echo "  # The restart script deletes this file"
echo '  if [ ! -f /tmp/epc.lock.$$ ]; then'
echo "     echo Lock file deleted. Sleeping"
echo "     break"
echo "  fi"
echo "done"
)> $SCRIPTFILE
chmod +x $SCRIPTFILE

# Delete any existing tmux session for srsepc started by this user.
tmux kill-session -t epc
tmux new-session -d -s epc "bash $SCRIPTFILE; sleep 300"
